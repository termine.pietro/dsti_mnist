import json
import paramiko
from zipfile import ZipFile 
import os 

class SSHClient:
    def __init__(self,user=None,key=None,address=None,verbose=False):
        try:
            self.keypath = str(key)
            self.address = str(address)
            self.user    = str(user)
        except Exception as ex:
            if(verbose):
                print("Invalid dataypes passed argumnets must be strings")
            raise ex
            
    def connect(self,user=None,key=None,address=None):
       try:
           if user is not None:
                self.keypath = str(key)
           if address is not None:
                self.keypath = str(key)
           self.user    = str(user)
       except Exception as ex:
            if(verbose):
                print("Invalid dataypes passed argumnets must be strings")  
            raise ex


def get_all_file_paths(directory): 

	# initializing empty file paths list 
	file_paths = [] 

	# crawling through directory and subdirectories 
	for root, directories, files in os.walk(directory): 
		for filename in files: 
			# join the two strings in order to form the full filepath. 
			filepath = os.path.join(root, filename) 
			file_paths.append(filepath) 

	# returning all file paths 
	return file_paths		 

def Unzip(file_name,folder=None,verbose=False):
    with ZipFile(file_name, 'r') as zip: 
    # printing all the contents of the zip file 
        zip.printdir() 
  
        # extracting all the files 
        if(verbose):
            print('Extracting all the files now...') 
        zip.extractall(path=folder)
        if(verbose):
            print('Done!') 

def Zip(filename,directory=None,verbose=False):
    try:
        # calling function to get all file paths in the directory 
        file_paths = get_all_file_paths(directory) 

        # printing the list of all files to be zipped 
        if(verbose):
            print('Following files will be zipped:') 
            for file_name in file_paths: 
                print(file_name) 

        # writing files to a zipfile 
        with ZipFile(filename,'w') as zip: 
            # writing each file one by one 
            for file in file_paths: 
                zip.write(file) 
        if(verbose):
            print('All files zipped successfully!')
    
    except Exception as ex:
        raise ex


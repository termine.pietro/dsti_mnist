#!/bin/sh
sudo sed -i "s/#\ conf_force_conffold=YES/conf_force_conffold=YES/g" /etc/ucf.conf
sudo apt -y update && sudo apt -y upgrade
sudo sed -i "s/conf_force_conffold=YES/#conf_force_conffold=YES/g" /etc/ucf.conf
sudo apt -y install apache2
sudo chmod 777 -R /var/www/html
sudo apt -y install python3-pip awscli
sudo apt -y install unzip
sudo apt -y install python3-pip
mkdir /home/ubuntu/.aws
cd /home/ubuntu/.aws
echo "[default]" >> credentials

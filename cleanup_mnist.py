#!/usr/bin/env python

import argparse
import logging
import os
import time
import sys

import boto3

import sys
import boto3
import json


def Init(filename_cleanup,filename_credentials,_aws):
    try:

        settings = {}
        credentials = {}
        with open(filename_credentials) as json_file:  
            credentials = json.load(json_file)

        with open(filename_cleanup) as json_file:  
            settings = json.load(json_file)
            #credentials = settings['credentials']
        for s in _aws:
            print("Acquiring handle to %s service" %(s))
            _aws[s] = boto3.resource(s,aws_access_key_id=credentials['access_id'], 
                                    aws_secret_access_key=credentials['access_key'], 
                                    region_name=credentials['region_name'])
    except Exception as e:
        print(str(e))
        raise
    return [_aws,settings,credentials]
    

def ec2_instance_cleanup(ec2,instance_ids):
    try:
        if not instance_ids or not ec2:
            return
        print('Removing EC2 Instance ({}) from AWS'.format(instance_ids))
        for ins in ec2.instances.filter(InstanceIds = instance_ids).all():
            ins.terminate()
            res = ins.wait_until_terminated('self',Filters=[{'Name':'instance-state-name','Values':['terminated']}])
   
    except Exception as e:
        print(str(e))    
        raise



def vpc_exists(ec2client, vpc_id):
    try:
        ec2client.describe_vpcs(VpcIds=[vpc_id])
    except Exception as e:
        print(str(e))
        return False
    return True


def delete_vpc(ec2,vpc_id, aws_region, release_eips=False):
    ec2client = ec2.meta.client
    if not vpc_exists(ec2client, vpc_id):
        print(f"VPC {vpc_id} does not exist in {aws_region}")
        return False

    # Exit cleanly if user did to specify at command line to delete EC2 instances for
    # a VPC with runnining instances
    filters = [
        {"Name": "instance-state-name", "Values": ["running"]},
        {"Name": "vpc-id", "Values": [vpc_id]},
    ]
    if ec2client.describe_instances(Filters=filters)["Reservations"]:
        print(
            f"Running EC2 instances exist in {vpc_id}. Please use --services ec2 to invoke the program."
        )
        return False

    vpc = ec2.Vpc(vpc_id)

    # delete transit gateway attachment for this vpc
    # note - this only handles vpc attachments, not vpn
    for attachment in ec2client.describe_transit_gateway_attachments()[
        "TransitGatewayAttachments"
    ]:
        if attachment["ResourceId"] == vpc_id:
            ec2client.delete_transit_gateway_vpc_attachment(
                TransitGatewayAttachmentId=attachment["TransitGatewayAttachmentId"]
            )

    # delete NAT Gateways
    # attached ENIs are automatically deleted
    # EIPs are disassociated but not released
    filters = [{"Name": "vpc-id", "Values": [vpc_id]}]
    for nat_gateway in ec2client.describe_nat_gateways(Filters=filters)["NatGateways"]:
        ec2client.delete_nat_gateway(NatGatewayId=nat_gateway["NatGatewayId"])

    # detach default dhcp_options if associated with the vpc
    dhcp_options_default = ec2.DhcpOptions("default")
    if dhcp_options_default:
        dhcp_options_default.associate_with_vpc(VpcId=vpc.id)

    # delete any vpc peering connections
    for vpc_peer in ec2client.describe_vpc_peering_connections()[
        "VpcPeeringConnections"
    ]:
        if vpc_peer["AccepterVpcInfo"]["VpcId"] == vpc_id:
            ec2.VpcPeeringConnection(vpc_peer["VpcPeeringConnectionId"]).delete()
        if vpc_peer["RequesterVpcInfo"]["VpcId"] == vpc_id:
            ec2.VpcPeeringConnection(vpc_peer["VpcPeeringConnectionId"]).delete()

    # delete our endpoints
    for ep in ec2client.describe_vpc_endpoints(
        Filters=[{"Name": "vpc-id", "Values": [vpc_id]}]
    )["VpcEndpoints"]:
        ec2client.delete_vpc_endpoints(VpcEndpointIds=[ep["VpcEndpointId"]])

    # delete custom security groups
    for sg in vpc.security_groups.all():
        if sg.group_name != "default":
            sg.delete()

    # delete custom NACLs
    for netacl in vpc.network_acls.all():
        if not netacl.is_default:
            netacl.delete()

    # ensure ENIs are deleted before proceding
    timeout = time.time() + 300
    filter = [{"Name": "vpc-id", "Values": [vpc_id]}]
    print(f"proceed with deleting ENIs")
    reached_timeout = True
    while time.time() < timeout:
        if not ec2client.describe_network_interfaces(Filters=filters)[
            "NetworkInterfaces"
        ]:
            print(f"no ENIs remaining")
            reached_timeout = False
            break
        else:
            print(f"waiting on ENIs to delete")
            time.sleep(30)

    if reached_timeout:
        print(f"ENI deletion timed out")

    # delete subnets
    for subnet in vpc.subnets.all():
        for interface in subnet.network_interfaces.all():
            interface.delete()
        subnet.delete()

    # Delete routes, associations, and routing tables
    filter = [{"Name": "vpc-id", "Values": [vpc_id]}]
    route_tables = ec2client.describe_route_tables(Filters=filter)["RouteTables"]
    for route_table in route_tables:
        for route in route_table["Routes"]:
            if route["Origin"] == "CreateRoute":
                ec2client.delete_route(
                    RouteTableId=route_table["RouteTableId"],
                    DestinationCidrBlock=route["DestinationCidrBlock"],
                )
            for association in route_table["Associations"]:
                if not association["Main"]:
                    ec2client.disassociate_route_table(
                        AssociationId=association["RouteTableAssociationId"]
                    )
                    ec2client.delete_route_table(
                        RouteTableId=route_table["RouteTableId"]
                    )
    # delete routing tables without associations
    for route_table in route_tables:
        if route_table["Associations"] == []:
            ec2client.delete_route_table(RouteTableId=route_table["RouteTableId"])

    # destroy NAT gateways
    filters = [{"Name": "vpc-id", "Values": [vpc_id]}]
    nat_gateway_ids = [
        nat_gateway["NatGatewayId"]
        for nat_gateway in ec2client.describe_nat_gateways(Filters=filters)[
            "NatGateways"
        ]
    ]
    for nat_gateway_id in nat_gateway_ids:
        ec2client.delete_nat_gateway(NatGatewayId=nat_gateway_id)

    # detach and delete all IGWs associated with the vpc
    for gw in vpc.internet_gateways.all():
        vpc.detach_internet_gateway(InternetGatewayId=gw.id)
        gw.delete()

    ec2client.delete_vpc(VpcId=vpc_id)
    return True
  

if __name__ == "__main__":
    aws_settings = {}
    aws_data_cleanup = {}
    filename_cleanp      = "cleanup.json"
    filename_credentials = "credentials.json"
    aws_settings = {}
    _aws = {'ec2':None,'s3':None}


    try:
        _aws,aws_data_cleanup,credentials = Init(filename_cleanp,filename_credentials,_aws)
        ec2 = _aws['ec2']
        s3  = _aws['s3']

        for key,val in aws_data_cleanup['ec2_instances'].items():
            ec2_instance_cleanup(_aws['ec2'],[val])
        if delete_vpc(ec2,vpc_id=aws_data_cleanup['vpc'], aws_region=credentials['region_name'], release_eips=False):
            print(f"destroyed {aws_data_cleanup['vpc']} in {credentials['region_name']}")
    except Exception as e:
        print(str(e))


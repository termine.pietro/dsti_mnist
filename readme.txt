This is a demo done for the dsti assignment for Amazon AWS

The main objective is to deploy automatically a webpage providing
digit recognition for drawn images. The webservice relys on a pre-trained 
cnn on mnist dataset.

The python scripts consist of deploy_mnist.py, cleanup_mnist.py and 
utilities. The scipts are based on Boto3 and use three json for configuring the  
ec2 instance (config.json), delete the instace (cleanup.json) and to store
the credential of the aws user(credential.json).

The deploy_mnist.py search for a bucket as provided in the json file, if not 
found will create it. Then cnn-minst.zip and requirement.txt are uploaded to 
the s3 bucket to be, then, downloaded from the ec2 instance/s.

The instance is then created, along with vpc, internet gateway and subnets 
and routing information. Once the ec2 instance is up the script awaits 
for the OK status, to be noted that at creation custom code updating the instance 
and installing apache server along with everything required is provided by webserver.sh.  

Once it is ok, the template of the front-end (html page) is loaded, modified inserting
the current ec2 instance public ip address and written over the index.html of the 
front-end. Then the webpage is zipped and uploaded to s3.

Then a ssh client is used to set the ec2 instance from the code. Here all things not set 
during the ec2 instance creation via the userdata (webserver.sh) are initialized:
cnn-minst.zip,requirement and the webpage are download from s3 into the ec2 instance, 
python updated according to the requiremnt.txt and files copied to the apache www folder
and finally a cron task is scheduled to start running the flask application.

During the process all the sensitive data are stored in cleanup.json, which is used 
by cleanup_mnist.py to remove the instace.

To be noted that this is just a very simple approach, lambda functions 
can be used and to move the ssh client data from the current code, removing the 
overall time required to get a valid ssh connection.

Also the key file *.pem has to be present in the current folder and the key value inserted
into the config.json 
 


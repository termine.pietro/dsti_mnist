import boto3
import json
import base64
import paramiko
import time
from Utilities  import *

def init(filename_config,filename_credentials,_aws):
    try:

        settings = {}
        credentials = {}
        with open(filename_credentials) as json_file:  
            credentials = json.load(json_file)

        with open(filename_config) as json_file:  
            settings = json.load(json_file)
            #credentials = settings['credentials']
        for s in _aws:
            print("Acquiring handle to %s service" %(s))
            access_id = credentials['access_id']
            access_key = credentials['access_key']
            region = credentials['region_name']
            _aws[s] = boto3.resource(s,aws_access_key_id=credentials['access_id'], 
                                    aws_secret_access_key=credentials['access_key'], 
                                    region_name=credentials['region_name'])
    except Exception as ex:
        #print(str(ex))
        raise ex
    return [_aws,settings,credentials]

# Create Bucket if doesnt exist and upload required files
def init_bucket(credentials,bucketName,listFiles):
    try :
        s3 = boto3.resource('s3',aws_access_key_id=credentials['access_id'], 
                                 aws_secret_access_key=credentials['access_key'], 
                                 region_name=credentials['region_name'])
        if(s3.Bucket(bucketName) not in s3.buckets.all()):
            s3.create_bucket(Bucket=bucketName, CreateBucketConfiguration={
            'LocationConstraint': credentials['region_name']})
        for f in listFiles:
            s3.meta.client.upload_file(f, bucketName, f)
    except Exception as e:
        print(str(e))
        raise  

# Create EC2 instance and deploy website
def deploy_mnist_service(ec2,s3,aws_settings,credentials,filename_cleanup='cleanup.json'):
    try:
        aws_data_cleanup = {}
        # create vpc 
        vpc = ec2.create_vpc(CidrBlock=aws_settings['vpc']['ip'])
        vpc.create_tags(Tags=[{"Key": "Name", "Value": aws_settings['vpc']['Name']}])
        vpc.wait_until_available()
        response = vpc.modify_attribute(EnableDnsHostnames={'Value': True})#,EnableDnsSupport={'Value': True},)
        #vpc.modify_vpc_attribute( EnableDnsSupport = { 'Value': True }, EnableDnsHostnames = { 'Value': True } )
        print('VPC created with id: '+vpc.id)
    
        aws_data_cleanup.update({'vpc': vpc.id})
    
        # create then attach internet gateway
        ig = ec2.create_internet_gateway()
    
        vpc.attach_internet_gateway(InternetGatewayId=ig.id)
        print('Internet Gateway created with id: '+ig.id)
        aws_data_cleanup.update({'igw': ig.id})
    
        # create a route table and a public route
        route_table = vpc.create_route_table()
        route = route_table.create_route(
            DestinationCidrBlock='0.0.0.0/0',
            GatewayId=ig.id
        )
        print('Created Route Table '+ route_table.id)
    
    
    
        aws_data_cleanup.update({'subnets': {}})
    
        for s in aws_settings['subnets']:
            subnet = ec2.create_subnet(CidrBlock=s['ip'], VpcId=vpc.id,)
            subnet.create_tags(Tags=[{"Key": "Name", "Value": s['Name']}])
            route_table.associate_with_subnet(SubnetId=subnet.id)
            aws_data_cleanup['subnets'].update({s['Name']:subnet.id})
            print('Created Subnet with id : '+ subnet.id+' and attached route table')
    
        # associate the route table with the subnet  
        aws_data_cleanup.update({'security_groups': {}})
        dump_sg = aws_data_cleanup['security_groups']
        # Create sec group
        for sec in aws_settings['security_groups']:
            sec_group = ec2.create_security_group(GroupName=sec['Name'], Description=sec['Description'], VpcId=vpc.id)
            dump_sg.update({sec['Name']:sec_group.id})
            # add rules 
            for rule in sec['auth']:
                if("Ip" in rule):
                    sec_group.authorize_ingress(CidrIp=rule['Ip'],IpProtocol=rule['IpProtocol'],FromPort=rule['FromPort'],ToPort=rule['ToPort'])
                elif("SecurityGroup" in rule):
                     sec_group.authorize_ingress(IpPermissions=[{'IpProtocol': rule['IpProtocol'],
                                                         'FromPort': rule['FromPort'],'ToPort': rule['ToPort'],
                                                         'UserIdGroupPairs': [{ 'GroupId': dump_sg[rule['SecurityGroup']] }] }])
    
        aws_data_cleanup.update({'ec2_instances': {}})
        ec2_inst = aws_data_cleanup['ec2_instances']
        print('Creating Instances...')
        user_data = ""
        for ins in aws_settings['instances']:
            with open(ins['UserData'], 'r') as f:
                for line in f:
                    user_data += line
            devs =[]
            # create credential file in the instance just created
            user_data += 'echo'+ ' "aws_access_key_id= '+credentials['access_id']+'" >> credentials\n'
            user_data += 'echo'+ ' "aws_secret_access_key= '+credentials['access_key']+'" >> credentials\n'
        
            net_interface =[{'SubnetId': aws_data_cleanup['subnets'][ins['subnet']], 'DeviceIndex': 0, 
                             'AssociatePublicIpAddress': ins['PublicIp'], 'Groups': [aws_data_cleanup['security_groups'][ins['SecurityGroup']]]}]
            instances = ec2.create_instances(BlockDeviceMappings = devs,NetworkInterfaces=net_interface,
                                             ImageId = ins['ImageId'], InstanceType=ins['InstanceType'], MinCount=1,MaxCount=ins['MaxCount'],
                                             KeyName = ins['keypair'],UserData=user_data)
            # wait for instance up and ready
            print('Instances created with id: '+instances[0].id)
            print('Waiting for Instance status OK... ')
            res = instances[0].wait_until_running('self',Filters=[{'Name':'instance-state-name','Values':['running']}])
            ec2_inst.update({ins['Name']:instances[0].id})
            waiter = ec2.meta.client.get_waiter('instance_status_ok')
            waiter.wait(InstanceIds=[instances[0].id])
        
            with open(filename_cleanup,'w+') as json_file:  
                json.dump(aws_data_cleanup,json_file)
    
            # Reload the instance attributes
            instance = ec2.Instance(instances[0].id)
            instance.load()
    
            print(instance.classic_address.public_ip)
        
            # Read in the file
            with open('./tmp/index_template.html', 'r') as file : #TODO
                filedata = file.read()
    
    
            # Replace the target string
            filedata = filedata.replace('xxx.xxx.xxx.xxx', instance.classic_address.public_ip)
    
            # Write the file out again
            with open('./MNIST/index.html', 'w') as file:
                file.write(filedata)
        
            # zip and upload file
            webpage_file = 'mnist.zip'
            Zip(webpage_file,'./MNIST')
            bucket_name = aws_settings['bucket']['Name']
            s3.meta.client.upload_file(webpage_file, bucket_name, webpage_file)
            # start ssh client: wait until ssh client ready
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            cmd_test = 'aws s3 ls s3://'+bucket_name
            bRunning = True
            keyfile = aws_settings['instances'][0]['keypair'] +'.pem'
            while (bRunning is True):
                try:
                    key = paramiko.RSAKey.from_private_key_file(keyfile) 
                    res = client.connect(hostname=instance.public_dns_name, username="ubuntu", pkey=key)
                    stdin, stdout, stderr = client.exec_command(cmd_test)
                    out_txt = stdout.read()
                    print ("Waiting for ssh client to be ready...")
                    client.close()
                    if(len(out_txt) <= 5):
                        time.sleep(15)
                    else:
                        bRunning = False
                except Exception as e:
                    print(str(e))
                    raise

            cmds = ['whoami','cd ~','aws s3 cp s3://'+bucket_name+'/mnist.zip mnist.zip',
                    'unzip mnist.zip -d ~/', 'cp -r  ~/MNIST/* /var/www/html/','rm -r  ~/MNIST',
                    'rm mnist.zip',
                    'aws s3 cp s3://'+bucket_name+'/requirements.txt requirements.txt',
                    'pip3 install -r requirements.txt ',
                    'aws s3 cp s3://'+bucket_name+'/cnn-mnist.zip cnn-mnist.zip',
                    'unzip cnn-mnist.zip -d ~/keras','rm cnn-mnist.zip','cp -r /home/ubuntu/keras/* /home/ubuntu/','pwd','rm -r keras',
                    'echo "cd /home/ubuntu/" >> keras_starter.sh', 'echo "mkdir -p hnnne" >> keras_starter.sh', 
                    'echo "python3 keras_flask.py &" >> keras_starter.sh', 'at -f /home/ubuntu/keras_starter.sh now + 2 min']
                    #'python3 keras_flask.py &'] # Connect/ssh to an instance
            try:
                # Here 'ubuntu' is user name and 'instance_ip' is public IP of EC2
                key = paramiko.RSAKey.from_private_key_file(keyfile) #TODO
                res = client.connect(hostname=instance.public_dns_name, username="ubuntu", pkey=key)
               
                for cmd in cmds:
                    # Execute a command(cmd) after connecting/ssh to an instance
                    stdin, stdout, stderr = client.exec_command(cmd)
                    print (stdout.read())
    
                # close the client connection once the job is done
                client.close()
            except (Exception) as e:
                #print(e)
                raise e
            print("Service avaiable at ip: "+instances[0].id)
            print(instance.public_dns_name)    
   
    except Exception as e:
        #print(str(e))
        raise e


if __name__ == "__main__":
    filename_config      = "config.json"
    filename_cleanp      = "cleanup.json"
    filename_credentials = "credentials.json"
    aws_settings = {}
    res = ['ec2','s3']
    _aws = {'ec2':None,'s3':None}


    try:
        _aws,aws_settings,credentials = init(filename_config,filename_credentials,_aws)
        ec2 = _aws['ec2']
        s3  = _aws['s3']

        bucket_name =  aws_settings['bucket']['Name']
        init_bucket(credentials,bucket_name,['cnn-mnist.zip','requirements.txt'])  #inser bucket name here TODO
        deploy_mnist_service(ec2,s3,aws_settings,credentials)
    except Exception as e:
        print(str(e))

 
